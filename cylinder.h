#pragma once
#include <vector>
#include <glm\glm.hpp>
#include "shape.h"

class Cylinder: public Shape
{
    public:
    Cylinder(float height, float radius, int gradation);

    private:
    void initializeVertices(float radius, int gradation, float height) override;
    void initializeIndices(int gradation) override;
};
