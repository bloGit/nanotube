#include "nanotube.h"

const int Nanotube::GRADATION {16};
const float Nanotube::SPHERE_RADIUS {0.2f};
const float Nanotube::CYLINDER_RADIUS {0.06f};

Nanotube::Nanotube(int size, int length): m_ringSize(size), m_length(length), m_sphere(SPHERE_RADIUS, GRADATION),
                                          m_cylinder(2.0f/Shape::SQRT3, CYLINDER_RADIUS, GRADATION)
{
    m_radius = 1 / (2 * std::sin(Shape::PI/m_ringSize/2));
}

void Nanotube::populateVectors(std::vector<float> & sphereVert, std::vector<float> & sphereNorm,
                               std::vector<float> & cylindVert, std::vector<float> & cylindNorm) const
{    
    m_sphere.populateVertices(sphereVert);
    m_sphere.populateNormals(sphereNorm);
    m_cylinder.populateVertices(cylindVert);
    m_cylinder.populateNormals(cylindNorm);    
}
