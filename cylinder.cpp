#include <cmath>
#include <vector>
#include <glm\glm.hpp>
#include "Cylinder.h"

Cylinder::Cylinder(float height, float radius, int gradation)
{
    m_indicesCount = gradation * TWO_TRIANGLES_VERTEX_NUM;
    initializeVertices(radius, gradation, height);
    initializeIndices(gradation);
}

void Cylinder::initializeVertices(float radius, int grad, float height)
{
    int verticesCount = (grad + 1) * 2;
    m_vertices.resize(verticesCount);
    m_normals.resize(verticesCount);

    bool toggle = true;
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j <= grad; j++)
        {
            float x = radius * std::sin(j * 2*PI / grad);
            float y = radius * std::cos(j * 2*PI / grad);

            m_vertices[i*(grad + 1) + j] = glm::vec3(x, y, i*height);
            m_normals[ i*(grad + 1) + j] = glm::vec3(x, y, 0);
        }
    }
}

void Cylinder::initializeIndices(int grad)
{
    m_indices.resize(m_indicesCount);
    for (int j = 0; j < grad; j++)
    {
        m_indices[TWO_TRIANGLES_VERTEX_NUM * j + 0] = j;
        m_indices[TWO_TRIANGLES_VERTEX_NUM * j + 1] = j + 1;
        m_indices[TWO_TRIANGLES_VERTEX_NUM * j + 2] = (grad + 1) + j;
        m_indices[TWO_TRIANGLES_VERTEX_NUM * j + 3] = j + 1;
        m_indices[TWO_TRIANGLES_VERTEX_NUM * j + 4] = (grad + 1) + j + 1;
        m_indices[TWO_TRIANGLES_VERTEX_NUM * j + 5] = (grad + 1) + j;
    }    
}