#pragma once
#include <array>
#include <stack>
#include <glm\gtc\matrix_transform.hpp>

#include "sphere.h"
#include "cylinder.h"

class Nanotube
{
    public:
    Nanotube(int size, int length);    

    void populateVectors(std::vector<float> & sphereVert, std::vector<float> & sphereNorm,
                         std::vector<float> & cylindVert, std::vector<float> & cylindNorm) const;

    using DrawFunction = void(int, int, const std::array<float*, 4> &);
    void renderNanotube(std::stack<glm::mat4> & matrixStack, DrawFunction drawVertices) const;

    private:        
    static const int GRADATION;
    static const float SPHERE_RADIUS;
    static const float CYLINDER_RADIUS;    
    
    int m_ringSize {0};
    int m_length {0};
    float m_radius {0.0f};
    Sphere m_sphere;
    Cylinder m_cylinder;

    std::array<float, 4> m_sphereAmbient  = {0.247f, 0.199f, 0.074f, 1}; //R, G, B, A
    std::array<float, 4> m_sphereDiffuse  = {0.751f, 0.606f, 0.226f, 1};
    std::array<float, 4> m_sphereSpecular = {0.628f, 0.555f, 0.366f, 1};
    float m_sphereShininess {51.2f};

    std::array<float, 4> m_cylinderAmbient  = {0.212f, 0.1275f, 0.054f, 1}; //R, G, B, A
    std::array<float, 4> m_cylinderDiffuse =  {0.714f, 0.4284f, 0.181f, 1};
    std::array<float, 4> m_cylinderSpecular = {0.393f, 0.2719f, 0.166f, 1};
    float m_cylinderShininess{25.6f};

    std::array<float*, 4> m_sphereColor
        = { m_sphereAmbient.data(), m_sphereDiffuse.data(), m_sphereSpecular.data(), &m_sphereShininess };

    std::array<float*, 4> m_cylinderColor
        = { m_cylinderAmbient.data(), m_cylinderDiffuse.data(), m_cylinderSpecular.data(), &m_cylinderShininess };
};

inline void Nanotube::renderNanotube(std::stack<glm::mat4> & mvStack, DrawFunction drawVertices) const
{
    for (int length = 0; length < m_length; length++)
    {
        for (int i = 0; i < m_ringSize; i++) //draw a full single ring
        {
            mvStack.push(mvStack.top());

                //draw a single repeatable segment: 2 atoms (spheres) and 3 bonds (cylinders):
                /*
                       \
                        O--O
                       /                
                */
                mvStack.top() *= glm::rotate(glm::mat4(1.0f), i*Shape::PI / m_ringSize * 2, glm::vec3(0.0, 0.0, 1.0));
                mvStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(m_radius, 0.0f, 0.0f));

                mvStack.push(mvStack.top());
                drawVertices(0, m_sphere.getVertexCount(), m_sphereColor);  //draw first atom
                mvStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 2.0/Shape::SQRT3));
                drawVertices(0, m_sphere.getVertexCount(), m_sphereColor);  //draw second atom
                mvStack.pop();

                drawVertices(2, m_cylinder.getVertexCount(), m_cylinderColor); //draw horizontal bond
                mvStack.push(mvStack.top());
                mvStack.top() *= glm::rotate(glm::mat4(1.0f), Shape::PI*2/3, glm::vec3(1.0, 0.0, 0.0));
                mvStack.top() *= glm::rotate(glm::mat4(1.0f), Shape::PI/m_ringSize/2.3f, glm::vec3(0.0, -1.0, 0.0));
                drawVertices(2, m_cylinder.getVertexCount(), m_cylinderColor); //draw diagonal 'decreasing' bond
                mvStack.pop();

                mvStack.top() *= glm::rotate(glm::mat4(1.0f), Shape::PI*4/3, glm::vec3(1.0, 0.0, 0.0));
                mvStack.top() *= glm::rotate(glm::mat4(1.0f), Shape::PI/m_ringSize/2.3f, glm::vec3(0.0, -1.0, 0.0));
                drawVertices(2, m_cylinder.getVertexCount(), m_cylinderColor); //draw diagonal 'increasing' bond

            mvStack.pop();
        }

        //shift next ring by one segment forward...
        mvStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 3.0/Shape::SQRT3));

        //...and by "half-segment up" to connect bonds to existing atoms
        mvStack.top() *= glm::rotate(glm::mat4(1.0f), Shape::PI/m_ringSize, glm::vec3(0.0, 0.0, 1.0));
    }
}


/* this is how 2 neighboring rings look like (m_length == 2, m_ringSize == 3):

                            \
                             O--O
                       \    /    
                        O--O     
                       /    \    
                             O--O
                       \    /    
                        O--O     
                       /    \                                                   
                             O--O
                       \    /    
                        O--O
                       /                                           

   *of course this is a FLAT picture - actual shape is wrapped into a closed tube
*/