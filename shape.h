#pragma once
#include <vector>
#include <glm\glm.hpp>

class Shape
{
    public:
    static const float PI;
    static const float SQRT3;

    void populateVertices(std::vector<float> & vertices) const;
    void populateNormals(std::vector<float> & normals) const;
    int getVertexCount() const;

    protected:
    Shape() {};
    virtual void initializeVertices(float radius, int gradation, float height) {};
    virtual void initializeIndices(int gradation) {};
    
    static const int TWO_TRIANGLES_VERTEX_NUM;

    int m_indicesCount {0};
    std::vector<int> m_indices;
    std::vector<glm::vec3> m_vertices;
    std::vector<glm::vec3> m_normals;
};

inline int Shape::getVertexCount() const
{
    return m_indicesCount;
}