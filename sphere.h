#pragma once
#include <vector>
#include <glm\glm.hpp>
#include "shape.h"

class Sphere: public Shape
{
    public:
    Sphere(float radius, int gradation);

    private:
    void initializeVertices(float radius, int gradation, float height = 0) override;
    void initializeIndices(int gradation) override;
};
