#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <stack>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include "utils.h"

#include "lightsource.h"
#include "nanotube.h"

//TODO: put following data to a separate class
const int VAO_COUNT {1};
const int VBO_COUNT {4};
GLuint vao[VAO_COUNT];
GLuint vbo[VBO_COUNT];
GLuint shader;
GLuint uni_modelView, uni_projection, uni_normal; //uniform-variable handles
glm::mat4 projectionMatrix, viewMatrix, invertedTransposedMat;
std::stack<glm::mat4> mv_matrixStack; //model-view matrix stack
GLuint materialColor[4];

LightSource light;
Nanotube nanotube(8, 56);

void reshapeWindow(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height); // sets screen region associated with the frame buffer
    const float aspect = static_cast<float>(width) / static_cast<float>(height);
    projectionMatrix = glm::perspective(glm::radians(60.0f), aspect, 0.1f, 100.0f);
}

void init(GLFWwindow* window)
{
    shader = createShaderProgram("vertex.glsl", "fragment.glsl");

    //initialize VBO buffers
    std::vector<float> sphereVertices, sphereNormals, cylinderVertices, cylinderNormals;
    nanotube.populateVectors(sphereVertices, sphereNormals, cylinderVertices, cylinderNormals);

    glGenVertexArrays(VAO_COUNT, vao);
    glBindVertexArray(vao[0]);
    glGenBuffers(VBO_COUNT, vbo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sphereVertices.size()*sizeof(float), &sphereVertices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sphereNormals.size()*sizeof(float), &sphereNormals[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
    glBufferData(GL_ARRAY_BUFFER, cylinderVertices.size()*sizeof(float), &cylinderVertices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
    glBufferData(GL_ARRAY_BUFFER, cylinderNormals.size()*sizeof(float), &cylinderNormals[0], GL_STATIC_DRAW);

    //get handles to all uniform variables
    uni_projection = glGetUniformLocation(shader, "proj_matrix");
    uni_modelView = glGetUniformLocation(shader, "mv_matrix");
    uni_normal = glGetUniformLocation(shader, "norm_matrix");
    materialColor[0] = glGetUniformLocation(shader, "material.ambient");
    materialColor[1] = glGetUniformLocation(shader, "material.diffuse");
    materialColor[2] = glGetUniformLocation(shader, "material.specular");
    materialColor[3] = glGetUniformLocation(shader, "material.shininess");

    // build perspective matrix
    int width {0}; int height {0};
    glfwGetFramebufferSize(window, &width, &height);
    reshapeWindow(window, width, height);
}

void drawVertices(int vboIndex, int vertexCount, const std::array<float*, 4> & colors)
{
    glProgramUniform4fv(shader, materialColor[0], 1, colors[0]);
    glProgramUniform4fv(shader, materialColor[1], 1, colors[1]);
    glProgramUniform4fv(shader, materialColor[2], 1, colors[2]);
    glProgramUniform1f(shader, materialColor[3], *colors[3]);

    glUniformMatrix4fv(uni_modelView, 1, GL_FALSE, glm::value_ptr(mv_matrixStack.top()));
    glBindBuffer(GL_ARRAY_BUFFER, vbo[vboIndex]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    invertedTransposedMat = glm::transpose(glm::inverse(mv_matrixStack.top()));
    glUniformMatrix4fv(uni_normal, 1, GL_FALSE, glm::value_ptr(invertedTransposedMat));
    glBindBuffer(GL_ARRAY_BUFFER, vbo[vboIndex + 1]);
    glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);
    glEnableVertexAttribArray(1);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LEQUAL);
    glFrontFace(GL_CW);
    glDrawArrays(GL_TRIANGLES, 0, vertexCount);
}

void display(GLFWwindow* window, float rotSpeed)
{
    glClear(GL_DEPTH_BUFFER_BIT);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(shader);
        
    glUniformMatrix4fv(uni_projection, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
                            
    viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 10.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    light.placeLight(shader, viewMatrix);

    mv_matrixStack.push(viewMatrix);
    
    static float flightPos = 0;    
    flightPos += 0.14f;
    mv_matrixStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -flightPos)); //forward flight
    if (flightPos > 6.0/Shape::SQRT3)
    {
        mv_matrixStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 6.0/Shape::SQRT3));        
        flightPos = 0;
    }

    //circular "camera" floating
    mv_matrixStack.top() *= glm::rotate(glm::mat4(1.0f), rotSpeed, glm::vec3(0.0, 0.0, 1.0));
    mv_matrixStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    mv_matrixStack.top() *= glm::rotate(glm::mat4(1.0f), -rotSpeed, glm::vec3(0.0, 0.0, 1.0));

    //tube rotation
    mv_matrixStack.top() *= glm::rotate(glm::mat4(1.0f), -rotSpeed, glm::vec3(0.0, 0.0, 1.0));
    
    nanotube.renderNanotube(mv_matrixStack, drawVertices);

    mv_matrixStack.pop();
}

int main(void)
{
    if (!glfwInit())
    {
        exit(EXIT_FAILURE);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    GLFWwindow* window = glfwCreateWindow(800, 800, "Nanotube animation", NULL, NULL);
    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK)
    {
        exit(EXIT_FAILURE);
    }
    glfwSwapInterval(1);

    glfwSetWindowSizeCallback(window, reshapeWindow);
    init(window);

    while (!glfwWindowShouldClose(window))
    {
        display(window, static_cast<float>(glfwGetTime()));
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}