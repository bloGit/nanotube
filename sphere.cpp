#include <cmath>
#include <vector>
#include <glm\glm.hpp>
#include "sphere.h"

Sphere::Sphere(float radius, int gradation)
{
    m_indicesCount = gradation * gradation * TWO_TRIANGLES_VERTEX_NUM;
    initializeVertices(radius, gradation);
    initializeIndices(gradation);
}

void Sphere::initializeVertices(float radius, int grad, float height)
{
    int verticesCount = (grad + 1) * (grad + 1);
    m_vertices.resize(verticesCount);
    m_normals.resize(verticesCount);

    for (int i = 0; i <= grad; i++)
    {
        for (int j = 0; j <= grad; j++)
        {
            float x = radius * std::cos(j * 2*PI / grad) * std::sin(i * PI / grad);
            float y = radius * std::sin(j * 2*PI / grad) * std::sin(i * PI / grad);
            float z = radius * std::cos(i * PI / grad);

            m_vertices[i*(grad + 1) + j] = glm::vec3(x, y, z);
            m_normals[ i*(grad + 1) + j] = glm::vec3(x, y, z);
        }
    }
}

void Sphere::initializeIndices(int grad)
{    
    m_indices.resize(m_indicesCount);
    for (int i = 0; i < grad; i++)
    {
        for (int j = 0; j < grad; j++)
        {
            m_indices[TWO_TRIANGLES_VERTEX_NUM * (i*grad + j) + 0] = i*(grad + 1) + j;
            m_indices[TWO_TRIANGLES_VERTEX_NUM * (i*grad + j) + 1] = i*(grad + 1) + j + 1;
            m_indices[TWO_TRIANGLES_VERTEX_NUM * (i*grad + j) + 2] = (i + 1)*(grad + 1) + j;
            m_indices[TWO_TRIANGLES_VERTEX_NUM * (i*grad + j) + 3] = i*(grad + 1) + j + 1;
            m_indices[TWO_TRIANGLES_VERTEX_NUM * (i*grad + j) + 4] = (i + 1)*(grad + 1) + j + 1;
            m_indices[TWO_TRIANGLES_VERTEX_NUM * (i*grad + j) + 5] = (i + 1)*(grad + 1) + j;
        }
    }
}