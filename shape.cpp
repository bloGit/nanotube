#include "shape.h"

const float Shape::PI = 3.141592f;
const float Shape::SQRT3 = 1.73205f;

const int Shape::TWO_TRIANGLES_VERTEX_NUM = 6;

void Shape::populateVertices(std::vector<float> & vertices) const
{
    for (int i = 0; i < m_indicesCount; i++)
    {
        vertices.push_back((m_vertices[m_indices[i]]).x);
        vertices.push_back((m_vertices[m_indices[i]]).y);
        vertices.push_back((m_vertices[m_indices[i]]).z);
    }
}

void Shape::populateNormals(std::vector<float> & normals) const
{
    for (int i = 0; i < m_indicesCount; i++)
    {
        normals.push_back((m_normals[m_indices[i]]).x);
        normals.push_back((m_normals[m_indices[i]]).y);
        normals.push_back((m_normals[m_indices[i]]).z);
    }
}
