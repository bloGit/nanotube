#pragma once
#include <array>

class LightSource
{
    public:    
    void placeLight(GLuint shaderProg, glm::mat4 vMatrix);

    private:
    glm::vec3 m_lightPosition {5.0f, 2.0f, 2.0f};
    
    //auxilary storage to keep converted (space view) position:
    glm::vec3 m_lightPosVector;
    float m_fLightPosition[3];

    //light color:
    std::array<float, 4> m_global   = { 0.7f, 0.7f, 0.7f, 1.0f };//R, G, B, A

    std::array<float, 4> m_ambient  = { 0.0f, 0.0f, 0.0f, 1.0f };
    std::array<float, 4> m_diffuse  = { 1.0f, 1.0f, 1.0f, 1.0f };
    std::array<float, 4> m_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
};

inline void LightSource::placeLight(GLuint shader, glm::mat4 vMatrix)
{
    static GLuint uni_global = glGetUniformLocation(shader, "globalAmbient");
    static GLuint uni_ambient = glGetUniformLocation(shader, "light.ambient");
    static GLuint uni_diffuse = glGetUniformLocation(shader, "light.diffuse");
    static GLuint uni_specular = glGetUniformLocation(shader, "light.specular");
    static GLuint uni_position = glGetUniformLocation(shader, "light.position");

    //convert light� position to the view space
    m_lightPosVector = vMatrix * glm::vec4(m_lightPosition, 1.0);

    //save it in a float array
    m_fLightPosition[0] = m_lightPosVector.x;
    m_fLightPosition[1] = m_lightPosVector.y;
    m_fLightPosition[2] = m_lightPosVector.z;

    glProgramUniform4fv(shader, uni_global, 1, m_global.data());
    glProgramUniform4fv(shader, uni_ambient, 1, m_ambient.data());
    glProgramUniform4fv(shader, uni_diffuse, 1, m_diffuse.data());
    glProgramUniform4fv(shader, uni_specular, 1, m_specular.data());
    glProgramUniform3fv(shader, uni_position, 1, m_fLightPosition);
}

